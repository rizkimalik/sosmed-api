<?php

header('content-type: application/json');
include_once "config.php";
include_once "../helper/logger.php";

// Receive incoming messages
$input = json_decode(file_get_contents('php://input'), true);

if (isset($input['object']) === 'page') {
    log_it("facebook/messenger", $input);
} 
else {
    // Verification request
    if ($_GET['hub_verify_token'] === $verify_token) {
        echo $_GET['hub_challenge'];
    } else {
        // Log the error
        log_error("facebook/messenger", "Verification Error: Invalid verify token");
    }
}

