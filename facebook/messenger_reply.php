<?php

include_once "config.php";
include_once "../helper/logger.php";

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $value = json_decode(file_get_contents('php://input'), true);
    $response = [
        'recipient' => ['id' => $value['id']],
        'message' => ['text' => $value['message']]
    ];

    // Send the response
    $ch = curl_init('https://graph.facebook.com/' . $graph_version . '/' . $value['page_id'] . '/messages?access_token=' . $value['access_token']);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($response));
    curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
    curl_exec($ch);
    curl_close($ch); 

    // $json = array(
    //     "status" => "success",
    //     "message" => $ch
    // );
    // echo json_encode($json);
    die();
}
else {
    http_response_code(405);
    echo "Method Not Allowed.";
}


