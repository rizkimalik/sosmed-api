<?php
function log_it($dir, $message)
{
    $dirlogs = $_SERVER['DOCUMENT_ROOT'] . "/sosmedapi/logs/" . date('Y-m-d') . '/' . $dir;
    if (!file_exists($dirlogs)) {
        mkdir($dirlogs, 0777, true);
    }
    $filename = $dirlogs . '/' . date('YmdHis') . '.json';
    // $message = '[' . date("Y-m-d H:i:s") . '] = ' . print_r($message, true) . PHP_EOL;
    $data = json_encode($message);
    file_put_contents($filename, $data, FILE_APPEND);
}

function log_error($dir, $message)
{
    // $dirlogs = $_SERVER['DOCUMENT_ROOT'] . "/logs";
    $dirlogs = "logs/" . date('Y-m-d') . '/' . $dir . "/log_error";
    if (!file_exists($dirlogs)) {
        mkdir($dirlogs, 0777, true);
    }
    $filename = $dirlogs . '/' . date('Ymd') . '.json';
    $message = '[' . date("Y-m-d H:i:s") . '] = ' . print_r($message, true) . PHP_EOL;
    // if you don't add `FILE_APPEND`, the file will be erased each time you add a log
    file_put_contents($filename, $message, FILE_APPEND);
}
